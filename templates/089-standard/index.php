<?php
/**
 * @author   	cg@089webdesign.de
 */
 
defined('_JEXEC') or die;
//include system
include_once(JPATH_ROOT . "/templates/" . $this->template . '/lib/system.php');
//include template Functions CG
include_once(JPATH_ROOT . "/templates/" . $this->template . '/template_functions.php');

?>
<!DOCTYPE html>
<html lang="de-de">
<head>
	<?php //CG: weitere Fonts zuerst via prefetch oder preload HIER einfügen dann fontface in CSS ?>
		<link rel="preload" as="font" crossorigin type="font/woff2" href="/templates/089-standard/fonts/quicksand-regular-webfont.ttf">	
	<?php 
	// including head
	include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/head.php');
	?>
	<link href="<?php print $this->baseurl . "/templates/" . "/089-standard/css/normalize.css"; ?>" rel="stylesheet" type="text/css" />
	<link href="<?php print $this->baseurl . "/templates/" . "/089-standard/css/responsive.css"; ?>" rel="stylesheet" type="text/css" />
	<link href="<?php print $this->baseurl . "/templates/" . "/089-standard/css/styles.css"; ?>" rel="stylesheet" type="text/css" />		
</head>

<body id="body" class="site <?php print $detectAgent . ($detect->isMobile() ? "mobile " : " ") . $body_class . ($layout ? $layout." " : '') . $option. ' view-' . $view. ($itemid ? ' itemid-' . $itemid : '') . $pageclass; ?>">

	<?php if($this->countModules('modal-news')) : ?>
		<jdoc:include type="modules" name="modal-news" style="none" />
	<?php endif; ?>

	<!-- Body -->
		<div id="wrapper" class="fullwidth site_wrapper">
			<?php			
		($frontpage) ? print '<div class="aboveTheFold">' : '';	
			// including menu
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/menu.php');
			
		if($this->countModules('header')) : 
				// including header
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/header.php');			
		endif;
			// including breadcrumb
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/breadcrumbs.php');		
									
			// including content
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/content.php');
		($frontpage) ? print '</div>' : '';	
			
			// including bottom
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom.php');	
			
			// including bottom2
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom2.php');
			
			// including footer
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/footer.php');				
			
			?>					
			
		</div>
	
	
	<jdoc:include type="modules" name="debug" style="none" />
	<script type="text/javascript" src="<?php print '/templates/' . $this->template . '/js/jquery.lazy.min.js';?>"></script>

	<script type="text/javascript">

		jQuery(document).ready(function() {

			<?php //CG: Modal News ?>
		    	jQuery('#closeModal').click('on', function(){
		    		jQuery('.modal-news').remove();
		        	sessionStorage.setItem('modal-news', true); 
		    	});
			    if(sessionStorage.getItem('modal-news') != null) { 
			    	jQuery('body').addClass('newsClicked');
			    }
		<?php //CG: <-- END  Modal News ?>			    
			jQuery(window).load(function(){
				<?php if($frontpage) : ?>
					jQuery('body').addClass('rdy');
				<?php endif;?>
			});

			<?php //CG: scroll Ani ?>
				jQuery(window).scroll(function() {
				    var windowBottom = jQuery(this).scrollTop() + jQuery(this).innerHeight();

				    <?php $offsetMinusValue = 400; (!$clientMobile) ? $offsetMinusValue = 300 : $offsetMinusValue = 800; //if($currentMenuID == 166) { $offsetMinusValue = 500;} //CG manche UNterseiten brauchen unterschiedliche Values, werdens mehr wie 2, dann Array! ?>

				    jQuery(".startFade").each(function() {
				        var objectBottom = (jQuery(this).offset().top + jQuery(this).outerHeight() - <?php print $offsetMinusValue;?> ); <?php // hier minus 150 damit es nicht so spät im Viewport erst faded, je nachdem Wert erhöhen, dann faded es "früher" ?>
				        if (objectBottom < windowBottom) { 
				            if (jQuery(this).css("opacity")==0) {
				            	jQuery(this).fadeTo(800,1); 
				            	jQuery(this).addClass('add-Ani');
				            }
				        }
				        if(jQuery(window).scrollTop() == 0 ) {
				        	jQuery(this).css("opacity", "0");
				        	jQuery(this).removeClass('add-Ani');	
				        } 
				    });
				}).scroll();

			<?php //CG scroll Down Ani ?>
			<?php if($frontpage && !$clientMobile) : ?>
				jQuery("a").on('click', function(event) {
				    if (this.hash !== "") {
				      event.preventDefault();
				      var hash = this.hash;
				      jQuery('html, body').animate({
				        scrollTop: jQuery(hash).offset().top
				      }, 800, function(){
				        window.location.hash = hash;
				      });
				    } // End if
				  });
			<?php endif; ?>

			<?php //CG: lazy:?>
			<?php $thresholdValue = 0;
				(!$clientMobile) ? $thresholdValue = "50" : $thresholdValue = "0";
			?>
				jQuery('.lazy').Lazy({
				    scrollDirection: 'vertical',
				    defaultImage: '/images/kfo-planegg-placeholder.jpg',
				    effect: "fadeIn",
          			effectTime: 500,
				    threshold: <?php print $thresholdValue; ?>,
				    visibleOnly: true,
				    onError: function(element) {
				        console.log('error loading ' + element.data('src'));
				    }
				});

				setTimeout(function(){ <?php //CG: wg Scroll Ani: in mitten der page reload erfordert scroll to Top, delay, weil anders scheint es nicht zu gehen ?>
					jQuery(this).css("opacity", "0");
				    jQuery(this).removeClass('add-Ani');
					jQuery(window).scrollTop(0);
				}, 1000);			
			
			<?php if($clientMobile) : ?>
				<?php //mobile menu open/close ?>
				jQuery('.btn-navbar').click("on", function() {
					jQuery('.nav-collapse.collapse').toggleClass('openMenu');
					jQuery('button.btn-navbar').toggleClass('btn-modify');
					jQuery('#wrapper').toggleClass('overlay');
				});
			<?php else: ?>
				jQuery(window).scroll(function () { <?php //CG: sticky ?>
					(jQuery(this).scrollTop() > 90) ? jQuery('#menuWrapper').addClass('sticky') : jQuery('#menuWrapper').removeClass('sticky');
				});			
			<?php endif; ?>
			<?php if($isPhone) : //scroll to Top: ?> 
					var scrollElement = '<div class="scroll-top-wrapper">&uarr;</div>';
					jQuery('#wrapper').after(scrollElement);			
				jQuery(function(){	 
						jQuery(document).on( 'scroll', function(){
					 
							if (jQuery(window).scrollTop() > 414) {
								jQuery('.scroll-top-wrapper').addClass('show');
							} else {
								jQuery('.scroll-top-wrapper').removeClass('show');
							}
						});	 
						jQuery('.scroll-top-wrapper').on('click', scrollToTop);
					});	 
					function scrollToTop() {
						verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
						element = jQuery('body');
						offset = element.offset();
						offsetTop = offset.top;
						jQuery('html, body').animate({scrollTop: offsetTop}, 350, 'linear');
					}			
			<?php endif; ?>

				jQuery('.breadcrumb li:nth-child(3) .pathway').on('click', function(e){ <?php //CG: parent-Menues nicht klickbar machen im breadcrumb ?>
							e.preventDefault();
				}); 		
				jQuery('li.parent>a').on('click', function(event){
						event.preventDefault();
						return false;
				});			

	});

	</script>
<?php if(!$clientMobile) : ?>
	<div id="resizeAlarm">
		<p>Das Fenster Ihres Webbrowsers ist zu klein - bitte vergrössern Sie ihr Browser-Fenster um die Inhalte sinnvoll darstellen zu können.</p>
	</div>
<?php endif; ?>	
</body>
</html>

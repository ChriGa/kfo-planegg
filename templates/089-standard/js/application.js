/**
 * @author   Free-Joomla-Templates.com
 * @copyright   Copyright (C) 2014 Free-Joomla-Templates.com. All rights reserved.
 * @URL http://http://free-joomla-templates.com/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

$('.dropdown-toggle').dropdown()
$('.collapse').collapse('show')
$('#myModal').modal('hide')
$('.typeahead').typeahead()
$('.tabs').button()
$('.tip').tooltip()
$(".alert-message").alert()
<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die;
?>
<?php if($frontpage && $isPhone) : ?>
	<header id="header" class="fullwidth">
		<jdoc:include type="modules" name="header-mobile" style="custom" />	
	</header>
<?php else: ?>
	<header id="header" class="fullwidth">
		<jdoc:include type="modules" name="header" style="custom" />	
	</header>
<?php endif; ?>
<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<footer class="footer" role="contentinfo">
	<div class="footer-wrap">							
		<div class="footer">
			<jdoc:include type="modules" name="footer" style="none" />
		</div>				
	</div>
</footer>
<div id="copyright" class="fullwidth">
	<div class="copyWrapper innerwidth">
		 <a class="imprLink" href="/impressum.html" title="Impressum KFO Planegg">Impressum</a> <a class="imprLink" href="/datenschutz.html">Datenschutz</a>
	</div>
	<p class="copyWrapper--date">&copy; <?php print date("Y") .' '. $sitename;?> </p>
</div>	
		
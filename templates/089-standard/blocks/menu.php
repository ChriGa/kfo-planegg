<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die; 

?>
<?php if(!$clientMobile) : ?>
  <div id="menuWrapper" class=" ">
<?php endif; ?>
    <nav class="navbar-wrapper innerwidth">
        <div class="logo--small">
          <a class="brand" href="<?php echo $this->baseurl; ?>">
          <?php echo $logo; ?>
            <?php if ($this->params->get('sitedescription')) : ?>
              <?php echo '<div class="site-description">' . htmlspecialchars($this->params->get('sitedescription')) . '</div>'; ?>
            <?php endif; ?>
          </a>
        </div>
        <div class="vcard"> 
            <p class="org">KFO-Planegg Dr. Sophie Schulz</p>
            <p class="tel "><a class="" href="tel:+498596585">Tel: 089 - 859 65 85</a></p>
            <p class="adr"><span class="street-address">Bahnhofstr. 41</span><br />
              <span class="postal-code">82152 </span><span class="region">Planegg</span>
             </p>
        </div>
        <div class="navbar">
          <div class="navbar--inner">
            <?php print ($detectAgent =="phone ") ? '<button type="button" class="btn btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">' : '<button type="button" class="btn btn-navbar">'; ?>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>                                
          <?php if ($this->countModules('menu')) : ?>
            <div class="nav-collapse collapse "  role="navigation">
              <jdoc:include type="modules" name="menu" style="custom" />
            </div>
          <?php endif; ?>
          </div>
        </div>
    </nav>
<?php if(!$clientMobile) : ?>
  </div>
<?php endif; ?>